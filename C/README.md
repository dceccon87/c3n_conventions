# C3N C Conventions

## Structure
For each C file, define an Header file.
C and Header files start with License and after a brief description of file.
```c
/******************************************************************************/
/* Copyright <YEAR> <PROJECT> contributors                                    */
	...
/******************************************************************************/
/**
 * @file	<FILE NAME>
 * @brief	<SHORT DESCRIPTION>
 * 
 * <MORE EXTENSIVE DESCRIPTION>
 */
```

At the end, insert an _end of file_ delimitation
```c
/*********************************END OF FILE**********************************/
```

If you don't need define some section elements, don't remove the section header! 

### Header files
_Define guard symbol_ is defined by [Google C++ #define guard](https://google.github.io/styleguide/cppguide.html#The__define_Guard) convention: `<PROJECT>_<PATH>_<FILE>_H_`

```c
#ifndef <GUARD SYMBOL>
#define <GUARD SYMBOL>

/* Includes *******************************************************************/

/* Definitions ****************************************************************/

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Functions ******************************************************************/

#endif /* <GUARD SYMBOL> */
```

### C files
```c
/* Includes *******************************************************************/

/* Definitions ****************************************************************/

/* Variables ******************************************************************/

/* Macros *********************************************************************/

/* Static functions definition ************************************************/

/* Functions ******************************************************************/

/* Static functions ***********************************************************/

```


## Naming convention
Convention rules is based on [GNU Coding Standards](https://www.gnu.org/prep/standards/html_node/) and some elements from [Google C++ Naming convention](https://google.github.io/styleguide/cppguide.html).

### Types reference
Types reference is an abbreviation appended to element name. Example:
```c
char *my_var_str = NULL;
```

#### Strings
* `str`: **String**, define a generic string type;
* `cstr`: **Char String**, classic C-Strings defined with `char` type; 
* `wstr`: **Wide String**, Wide String defined with `wchat_t` type.

#### Numbers
* `sz`: Define the size of element (type is always `size_t`);
* `len`: Define the string length (type is always `size_t`);
* `cnt`: Define a counter (type it's often `size_t`);
* `n`: Variable is an integer;
* `un`: Variable is an unsigned integer;
* `f`: Variable is a float;
* `i`: Varible is an index integer (usually a `size_t` type);

#### Structures and objects
* `obj`: Variable is a custum structure reference. If structure define a standard object use the object name reference, examples:
	* `json`: Variable is a JSON object reference;
	* `xml`: Variable is a XML object reference;
* `buf`: Variable is a buffer array (1-dimension array);
* `lst`: Variable is a list reference (1-dimension array, any list structure);
* `map`: Variable is a map reference.

#### Examples
```c
const char *users_data_cstr = "{\"users\":[{\"name\":\"Mario\"}, {\"name\":\"Luigi\"}]}";
size_t users_data_cstr_len = strlen(users_data_cstr);

//This example use the JSON Parson library (@see https://github.com/kgabis/parson)
JSON_Value *users_data_json = json_parse_string(users_data_cstr);
```

### Function
#### Name
Function name can be defiend with convention similar of ``C++`` namespace (see [Google C++ Namespace names convention](https://google.github.io/styleguide/cppguide.html#Namespace_Names)). Each level name is separated by underscore ``_`` character.
```c
<type>
[project_]area_..._verb_operation[_type of arguments](arguments);
```
__project__ can be omitted if name cannot generate conflict.

##### Examples
```c
c3n_str_result_t
c3n_cstr_content_is_not_empty(char *str);
```
* Project: **c3n**;
* Area: **cstr_content**;
* Verb: **is_not**;
* Operation: **empty**;
* Function operation type: none.

#### Groups
If it's required define many function with the same name, need to define:
* A main generic function name;
* Derivated functions name.

##### Examples
```c
//Main function
c3n_str_result_t
c3n_cstr_content_is_not_empty(char *str);

//Derivated function with extra size argument
c3n_str_result_t
c3n_cstr_content_is_not_empty_len(char *str, size_t str_len);

//Derivated function with size and index argument
c3n_str_result_t
c3n_cstr_content_is_not_empty_ilen(char *str, size_t str_i, size_t str_len);
```

#### Verbs
Define a verb for specify function operation type:
* **init**: Initialize;
* **free**: Free allocated object;
* **clean**: Set elements to default values;
* **reset**: Reset to initilize status;
* **is**: Compare;
* **get**: Get operation;
* **set**: Set operation;

Extra verbs operation append:
* **not**: negate operation result;
* **iff** (_If only if_): do the operation only if a condition is respected.


## Function handling
Mainly: preserve memory allocation!

### Memory manage
Define function structure by [ISO/IEC TR 24731-2:2010](https://www.iso.org/standard/51678.html) model: Callee allocates, caller frees.

#### Parameters
If function initialize memory values (allocation), set the last parameters as the reference of output pointer.
specify in function documentation what are input and output parameters.

#### Example
```c
/**
 * A function for copy a string... 
 *
 * @param	source[in]	Source string to copy.
 * @param	source_len[in]	Source string length.
 * @param	dest[out]	Reference to destination string.
 * @param	dest_len[out]	Destination string length.
 * @return	C3N_STR_SUCCES if source string is copied to destination string. C3N_STR_FAIL is source string is NULL. C3N_STR_ERROR if is not possibile copy the string (memory allocation error).
 */
c3n_str_result_t
c3n_cstr_copy_sz(char *source, size_t source_len, char **dest, size_t *dest_len);
```

### Return values
Define a function return code values by:
* ``enum``: if results code is static for all functions group;
* define static values (``#define``): if result can change by functions group; but:
	* Define unsigned int values;
	* Use same return type for each function.

Base code values:
* ``SUCCESS``: function operation is ok. Value is ``EXIT_SUCCESS`` (``0``);
* ``FAIL``: function operation is done, but result isn't ok. Value is ``EXIT_FAILURE`` (``1``); 
* ``ERROR``: function operation generic error (for example, wrong parameters values). Value is ``EXIT_FAILURE + 1`` (``2``).
	* Any other values greater than ``ERROR`` can be used for describe the error type.

#### Example
```c
//Header
typedef enum
{
  C3N_STR_SUCCES = EXIT_SUCCESS,	/**<	Operation result done	*/
  C3N_STR_FAIL = EXIT_FAILURE,		/**<	Operation result fail	*/
  C3N_STR_ERROR = EXIT_FAILURE + 1	/**<	Operation end with error/s	*/
} c3n_str_result_t;

//C
c3n_str_result_t
c3n_cstr_print_sz(char *str, size_t str_len)
{
	//Error status
	if(NULL == str)
	{
		return C3N_STR_ERROR;
	}

	if(str_len > 0)
	{
		printf("%s\n", str);
		return C3N_STR_SUCCES;
	}
	return C3N_STR_FAIL;
}
```

## Documentation
Comment each element by [Doxygen C-like languages](https://www.stack.nl/~dimitri/doxygen/manual/docblocks.html#cppblock) convention.
